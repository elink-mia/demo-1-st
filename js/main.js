;
(function() {

    'use strict';

    // iPad and iPod detection  
    var isiPad = function() {
        return (navigator.platform.indexOf("iPad") != -1);
    };

    var isiPhone = function() {
        return (
            (navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPod") != -1)
        );
    };

    var burgerMenu = function() {
        $('.menu').click(function() {
            $('.offcanvas-collapse').toggleClass('open');
            if ($('.offcanvas-collapse').hasClass('open')) {
                $('.burger').addClass('active');
                $(".sub-menu-parent .sub-menu").removeClass('active');
            } else {
                $('.burger').removeClass('active');
            }
            $('body').toggleClass("no-scroll");
        });
    };

    var hover = function() {
        var $win = $(window).width();
        if ($win > 992) {
            $(".sub-menu-parent").hover(function() {
                    //Add a class of current and fade in the sub-menu
                $(this).find(".sub-menu").css({
                    'width': '120%',
                    'display': 'block',
                    'opacity': '1',
                    'z-index': '1',
                    'transform': 'translateY(0%)'
                });
            }, function() {
                $(this).find(".sub-menu").css('display', 'none');
            });
        }
        if ($win < 992) {
            $(".sub-menu-parent").click(function() {
                if ($(this).find('.sub-menu').hasClass('active') ) {
                    $(this).find('.sub-menu').removeClass('active');
                } else {
                    $('.sub-menu.active').removeClass('active');
                    $(this).find('.sub-menu').addClass('active');    
                }
            });
        }
    };

    var header = function() {
        $(document).ready(function() {
            var headerH = $('#header').height(),
                winW = $(window).width();
            if (winW < 992) {
                $('.offcanvas-collapse').css('top', headerH);
            }
        });
    };

    new WOW().init();

    var goToTop = function() {
        $('.js-gotop').on('click', function(event) {

            event.preventDefault();

            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');

            return false;
        });

        $(window).scroll(function() {
            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('#side-btn').addClass('active');
            } else {
                $('#side-btn').removeClass('active');
            }
        });
    };
    $(function() {
        burgerMenu();
        hover();
        header();
        goToTop();
    });

}());