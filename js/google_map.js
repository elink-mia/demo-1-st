var google;

function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 18,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(24.137508, 120.666916),

        // How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{
                "featureType": "landscape.natural",
                "stylers": [{
                    "color": "#bcddff"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#5fb3ff"
                }]
            },
            {
                "featureType": "road.arterial",
                "stylers": [{
                    "color": "#ebf4ff"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ebf4ff"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.stroke",
                "stylers": [{
                        "visibility": "on"
                    },
                    {
                        "color": "#93c8ff"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#c7e2ff"
                }]
            },
            {
                "featureType": "transit.station.airport",
                "elementType": "geometry",
                "stylers": [{
                        "saturation": 100
                    },
                    {
                        "gamma": 0.82
                    },
                    {
                        "hue": "#0088ff"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#1673cb"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.icon",
                "stylers": [{
                        "saturation": 58
                    },
                    {
                        "hue": "#006eff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#4797e0"
                }]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#209ee1"
                    },
                    {
                        "lightness": 49
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#83befc"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#3ea3ff"
                }]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                        "saturation": 86
                    },
                    {
                        "hue": "#0077ff"
                    },
                    {
                        "weight": 0.8
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [{
                        "hue": "#0066ff"
                    },
                    {
                        "weight": 1.9
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [{
                        "hue": "#0077ff"
                    },
                    {
                        "saturation": -7
                    },
                    {
                        "lightness": 24
                    }
                ]
            }
        ]
    };
    var mapOptions2 = {
        // How zoomed in you want the map to start at (always required)
        zoom: 16,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(23.498047, 120.381807),

        // How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{
                "featureType": "landscape.natural",
                "stylers": [{
                    "color": "#bcddff"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#5fb3ff"
                }]
            },
            {
                "featureType": "road.arterial",
                "stylers": [{
                    "color": "#ebf4ff"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ebf4ff"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.stroke",
                "stylers": [{
                        "visibility": "on"
                    },
                    {
                        "color": "#93c8ff"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#c7e2ff"
                }]
            },
            {
                "featureType": "transit.station.airport",
                "elementType": "geometry",
                "stylers": [{
                        "saturation": 100
                    },
                    {
                        "gamma": 0.82
                    },
                    {
                        "hue": "#0088ff"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#1673cb"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.icon",
                "stylers": [{
                        "saturation": 58
                    },
                    {
                        "hue": "#006eff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#4797e0"
                }]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#209ee1"
                    },
                    {
                        "lightness": 49
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#83befc"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#3ea3ff"
                }]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                        "saturation": 86
                    },
                    {
                        "hue": "#0077ff"
                    },
                    {
                        "weight": 0.8
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [{
                        "hue": "#0066ff"
                    },
                    {
                        "weight": 1.9
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [{
                        "hue": "#0077ff"
                    },
                    {
                        "saturation": -7
                    },
                    {
                        "lightness": 24
                    }
                ]
            }
        ]
    };
    // Get the HTML DOM element that will contain your map 
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var icon = {
        // url: 'http://www.clker.com/cliparts/T/i/k/0/I/6/google-map-grey-marker-md.png', // url
        scaledSize: new google.maps.Size(60, 60) // scaled size
    };
    // Let's also add a marker while we're at it
    var map = new google.maps.Marker({
        position: new google.maps.LatLng(24.137508, 120.666916),
        map: map,
        icon: true,
        title: '松泰企業社'
    });
}
google.maps.event.addDomListener(window, 'load', init);